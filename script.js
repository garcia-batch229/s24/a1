// Exponent Operator
let getCube = 2 ** 3;


// Template Literals
console.log(`The cube of 2 is ${getCube}`);
// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

let [apartmentNumber,street,city,zipCode] = address;
console.log(`I live at ${apartmentNumber} ${street}, ${city}, ${zipCode}`);

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {name,species,weight,measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`)

	//Arrow Functions
let numbers = [1, 2, 3, 4, 5];

// let forEach = numbers.forEach(function (num1) {
// 	console.log(num1);
// })

numbers.forEach((x) => {
	console.log(x);
})


// Javascript Classes

class Dog {
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let dog1 = new Dog('Frankie', 5, 'Miniature Dachshund');
let dog2 = new Dog('Jamies', 2, 'Siberian Husky');
console.log(dog1);
console.log(dog2);